#include "pch.h"
#include <string>
#include "FBullCowGame.h"
#include <map>
#define TMap std::map

FBullCowGame::FBullCowGame() { Reset(); } //default constructor


int32 FBullCowGame::GetCurrentTry() const { return MyCurrentTry; }
int32 FBullCowGame::GetHiddemWordLength() const { return MyHiddenWord.length(); }
bool FBullCowGame::IsGameWon() const { return bIsGameWon; }

int32 FBullCowGame::GetMaxTries() const 
{
	TMap<int32, int32> WordLengthToMaxTries{ {3,4}, {4,7}, {5,10}, {6,15}, {7, 20} };
	return WordLengthToMaxTries[MyHiddenWord.length()];
}

void FBullCowGame::Reset()
{
	const FString HIDDEN_WORD = "ant";
	MyHiddenWord = HIDDEN_WORD;

	bIsGameWon = false;

	MyCurrentTry = 1;
	return;
}



EGuessStatus FBullCowGame::CheckGuessValidity(FString Guess)
{

	if (!IsIsogram(Guess))
	{
		return EGuessStatus::Not_Isogram;
	}
	else if (!IsLowercase(Guess))
	{
		return EGuessStatus::Not_Lowecase;
	}
	else if (Guess.length() != GetHiddemWordLength())
	{
		return EGuessStatus::Wrong_Length;
	}
	else {
		return EGuessStatus::OK;
	}

}


// recieves a VALID guess, increments turns, and returns count
FBullCowCount FBullCowGame::SubmitValidGuess(FString Guess)
{
	MyCurrentTry++;
	FBullCowCount BullCowCount;

	int WordLength = MyHiddenWord.length();
	for (int32 GWChar = 0; GWChar < WordLength; GWChar++)
	{
		for (int32 MHWChar = 0; MHWChar < WordLength; MHWChar++)
		{
			if (Guess[GWChar] == MyHiddenWord[MHWChar])
			{
				if (GWChar == MHWChar)
				{
					BullCowCount.Bulls++;
				}
				else
				{
					BullCowCount.Cows++;
				}
			}
		}
	}
	if (BullCowCount.Bulls == GetHiddemWordLength()) {
		bIsGameWon = true;
	}
	else
	{
		bIsGameWon = false;
	}
	return BullCowCount;
}

bool FBullCowGame::IsIsogram(FString Word) const
{
	if (Word.length() <= 0) { return true; }
	
	TMap<char, bool> LetterSeen;
	for (auto Letter : Word) //for all letters of the word
	{
		Letter = tolower(Letter);
		if (LetterSeen[Letter]) {
			return false;
		}
		else {
			LetterSeen[Letter] = true;
		}
	}

}

bool FBullCowGame::IsLowercase(FString Word) const
{
	for (auto Letter : Word) //for all letters of the word
	{
		if (!iswlower(Letter)) {
			return false;
		}
	}
	return true;
}



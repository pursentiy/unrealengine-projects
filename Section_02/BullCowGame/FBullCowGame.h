#pragma once
#include <string>

using FString = std::string;
using int32 = int;

struct FBullCowCount
{
	int32 Bulls = 0;
	int32 Cows = 0;
};

enum class EGuessStatus
{
	Invalid_status,
	OK,
	Not_Isogram,
	Not_Lowecase,
	Wrong_Length
};



class FBullCowGame
{
public:
	FBullCowGame(); //constructor

	int32 GetMaxTries() const;
	int32 GetCurrentTry() const;
	int32 GetHiddemWordLength() const;
	bool IsGameWon() const;
	EGuessStatus CheckGuessValidity(FString); //TODO make a more rich return value;

	void Reset(); //TODO make a more rich return value
	

	// counts bulls & cows, and increases try # assuming valid guess
	FBullCowCount SubmitValidGuess(FString);

private:
	// see constructor for initialization
	int32 MyCurrentTry;
	int32 MyMaxTries;
	bool bIsGameWon;
	FString MyHiddenWord;

	bool IsIsogram(FString) const;
	bool IsLowercase(FString) const;
};